
name := "scala-learning-project"

version := "1.0"

scalaVersion := "2.12.2"


libraryDependencies += "com.typesafe" % "config" % "1.3.1"

libraryDependencies += "com.h2database" % "h2" % "1.4.195"

libraryDependencies += "com.github.pureconfig" % "pureconfig_2.12" % "0.7.0"

libraryDependencies += "org.jsoup" % "jsoup" % "1.10.2"

package net.eldiosantos.scala.learning.crawler.service

import net.eldiosantos.scala.learning.crawler.model.behavior.TargetObject
import net.eldiosantos.scala.learning.crawler.model.{Page, Target}

/**
  * Created by esjunior on 27/04/2017.
  */
class FetchData {

  def start(start: Target): List[Page] = {
    return TargetObject(start).fetch()
  }

}

package net.eldiosantos.scala.learning.crawler.model.behavior

import net.eldiosantos.scala.learning.crawler.model.Target

/**
  * Created by esjunior on 27/04/2017.
  */
object TargetObject {
  def apply(url: String, howDeep: Int) = new Target(url, howDeep) with TargetTrait
  def apply(url: String) = new Target(url, 0) with TargetTrait
  def apply(target: Target): Target with TargetTrait = this.apply(target.url, target.howDeep)
}
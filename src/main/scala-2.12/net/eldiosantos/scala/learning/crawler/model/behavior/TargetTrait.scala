package net.eldiosantos.scala.learning.crawler.model.behavior

import java.net.URL

import net.eldiosantos.scala.learning.crawler.model.Page

/**
  * Created by esjunior on 27/04/2017.
  */
trait TargetTrait {

  val url: String
  val howDeep: Int

  def fetch(): List[Page] = {
    val target = new URL(url)
    val page = Page(target)
    if(howDeep > 0) {
      printf("+|%s|%n", url)
      page :: page.getLinks(howDeep - 1)
        .map(t => TargetObject(t))
        .flatMap(t => t.fetch())
        .toList
    } else {
      printf("-|%s|%n", url)
      List(page)
    }
  }
}

package net.eldiosantos.scala.learning.crawler.repository

import java.sql.{Connection, DriverManager}

import net.eldiosantos.scala.learning.crawler.config.Cfg
import net.eldiosantos.scala.learning.crawler.model.Page

/**
  * Created by esjunior on 27/04/2017.
  */
class Database(connection: Connection) {

  def insert(page: Page) = {
    val st = connection.prepareStatement("insert into PAGE (url, content) values (?, ?)")

    st.setString(1, page.url.toExternalForm)
    st.setString(2, page.content)

    st.execute()
  }
}

object Database {
  val _dbConf = Cfg.config.database

  Class.forName(_dbConf.driver)
  val _conn = DriverManager.getConnection(_dbConf.url, _dbConf.user, _dbConf.pass)

  try {
    _conn.createStatement().executeQuery(_dbConf.test)
  } catch {
    case (e: Exception) => {
      println("Creating database structure...")
      val st = _conn.createStatement()
      //st.addBatch(_dbConf.create)
      _dbConf.create.split(";").foreach(st.execute(_))
      //val results = st.executeBatch()

      println("Database structure creation finished...")
      //results.foreach(r => println("result: " + r))
    }
  }


  val _database = new Database(_conn)

  def database(): Database = _database
}

package net.eldiosantos.scala.learning.crawler.model

import java.net.URL

import net.eldiosantos.scala.learning.crawler.model.behavior.PageTrait
import org.jsoup.Jsoup

/**
  * Created by esjunior on 27/04/2017.
  */
case class Page(val url: URL, val content: String) extends PageTrait {
}

object Page {
  def apply(url: URL) = {
    try {
      new Page(
        url
        , Jsoup.connect(url.toString).ignoreContentType(true).ignoreHttpErrors(true).get().toString
      )
    } catch {
      case e: Exception => new Page(url, e.getMessage)
    }
  }

  def apply(url: String, content: String): Page = new Page(new URL(url), content)
}


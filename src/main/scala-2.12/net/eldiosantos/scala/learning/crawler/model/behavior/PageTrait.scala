package net.eldiosantos.scala.learning.crawler.model.behavior

import java.net.URL

import net.eldiosantos.scala.learning.crawler.model.Target
import org.jsoup.Jsoup

import scala.collection.JavaConverters

/**
  * Created by esjunior on 27/04/2017.
  */
trait PageTrait {
  val content: String
  val url: URL
  def getLinks(howDeep: Int): Set[Target] = {

    printf("[%d] Fetching links for: %s%n", howDeep, url.toExternalForm)

    JavaConverters.asScalaIterator(Jsoup.parse(content).body().getElementsByTag("a").listIterator())
      .map(a => {
        val _url = a.attr("href")
        new Target(
          if(_url.startsWith("http")) _url
          else s"${url.getProtocol}://${url.getHost()}/${_url}"
          , howDeep
        )
      })
      .toSet
  }
}

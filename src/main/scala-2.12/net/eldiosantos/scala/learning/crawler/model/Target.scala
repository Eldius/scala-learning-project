package net.eldiosantos.scala.learning.crawler.model

import net.eldiosantos.scala.learning.crawler.model.behavior.TargetTrait

/**
  * Created by esjunior on 27/04/2017.
  */
class Target(_url: String, _howDeep: Int) {
  val url = _url
  val howDeep = _howDeep
}

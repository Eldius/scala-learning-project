package net.eldiosantos.scala.learning.crawler

import net.eldiosantos.scala.learning.crawler.config.Cfg
import net.eldiosantos.scala.learning.crawler.service.FetchData

/**
  * Created by esjunior on 26/04/2017.
  */
object MainObjectCrawler extends App {
  println("Starting me...")

  //println(Cfg.config)

  new FetchData().start(Cfg.start())
}

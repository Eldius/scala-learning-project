package net.eldiosantos.scala.learning.crawler.config

import net.eldiosantos.scala.learning.crawler.model.Target
import pureconfig._
import pureconfig.error.ConfigReaderFailures

/**
  * Created by esjunior on 26/04/2017.
  */
final case class Cfg(database: DatabaseConfig, startUrl: String, howDeep: Int)

final case class DatabaseConfig (url: String, driver: String, user: String, pass: String, create: String, test: String)

object Cfg {
  val _config: Cfg = loadConfigOrThrow[Cfg]("app-conf")

  def config(): Cfg = _config

  def start(): Target = new Target(_config.startUrl, _config.howDeep)
}